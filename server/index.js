const express = require("express");
const http = require("http");
const socketio = require("socket.io");
const cors = require("cors");

const {addUser,removeUser,getUser,getUserInRoom} = require('./users.js')

//express -> http -> socket.io
const app = express();
const server = http.createServer(app);
const io = socketio(server)

//connnection or connect / disconnect is KEYWORD !!!
//connect le got socket parameter
io.on("connection",(socket)=>{
    //callback is for error handling ,and will send back when  there is error occur
    socket.on('join',({name,room} ,callback )=>{
       
        const {error,user} = addUser({id:socket.id,name,room})
        if(error) return callback(error)

        //socket.emit is mcm no include the 2nd person ,the message welcome below is only can see by me
        socket.emit("message",{user:"admin",text:"Welcome "+user.name+" join the room "+user.room})
        //broadcast will sent to everyone who inside to the specific room !!!
        socket.broadcast.to(user.room).emit("message",{user:"admin",text:user.name+" has joined"})

        //join room 
        socket.join(user.room)

        io.to(user.room).emit("roomData",{room:user.room,users:getUserInRoom(user.room)})
        
        //due to no error so no nid parameter error
        callback();
    })

    socket.on("sendMessage",(message,callback)=>{
        console.log("1:",socket.id)
        const user = getUser(socket.id)
        console.log("her",user)
        io.to(user.room).emit("message",{user:user.name , text:message})
        io.to(user.room).emit("roomData",{room:user.room,users:getUserInRoom(user.room)})

        callback()
    })

    //disconnect no parameter due to u disconnect liao
    socket.on("disconnect",()=>{
        const user = removeUser(socket.id)

        if(user){
            io.to(user.room).emit("message",{user:"admin",text:user.name+" has left"})
        }
    })
})

const router = require('./router');
const PORT = process.env.PORT || 5000
app.use(router)
app.use(cors())

server.listen(PORT,()=>console.log('Running Server on port :'+ PORT))
